/**
 * @file
 * JS/Ext file for viewer of Chado tables
 * 
 */

  Ext.namespace('genes4all_viewer');
  
  this.Panel_Interact = new Ext.Panel({
      layout : 'fit',
      border : true,
      title : 'Interact',
      region : "west",
      split : true,
      width : 200,
      collapsible : true,
      headerAsText : true,
      activeTab : 1,
      items : [
      TabPanel_Interact
      ]
  });

  this.TabPanel_Data = new Ext.TabPanel({
      activeTab : 0,
      items : [
      this.GridPanel_DataSelected,
      this.GridPanel_DataGroups
      ]
  });

  
  this.Panel_Data = new Ext.Panel({
      layout : 'fit',
      border : true,
      title : 'Data',
      region : "east",
      split : true,
      width : 200,
      collapsible : true,
      headerAsText : true,
      activeTab : 0,
      items : [
      this.TabPanel_Data
      ]
  });
  

  
  this.MainViewport = new Ext.Viewport({
      title : 'genes4all_viewer - CSIRO Ecosystem Sciences',
      layout : 'accordion',
      defaults : {
          bodyStyle : 'padding : 15px'
      },
      layoutConfig : {
          titleCollapse : true,
          animate : true,
          activeOnTop:false
      },
      items : [{
          title : 'genes4all_viewer',
          layout : "border",
          deferredRender : false,
          items : [
          this.Panel_Interact,
          this.Panel_Map,
          this.Panel_Data
          ]
      }]
  });
