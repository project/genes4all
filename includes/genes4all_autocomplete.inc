<?php
// $Id$

/**
 * @file
 * Autocomplete functions use in genes4all and submodules
 */

/**
 * Autocomplete features
 *
 * @see user_autocomplete
 *
 */
function genes4all_feature_autocomplete($string = '') {
  $matches = array();
  gmod_dbsf_db_execute('chado');
  if (!empty($string)) {
    $result = db_query_range(
      "SELECT feature.uniquename FROM feature feature join cvterm as cvterm ON "
    . " cvterm.cvterm_id=feature.type_id "
    . " WHERE LOWER(feature.uniquename) LIKE LOWER('%s%%') AND cvterm.name IN ('ORF','polypeptide','contig','gene','mRNA','CDS')", $string, 0, 10
    );
    while ($res = db_fetch_object($result)) {
      $matches[$res->uniquename] = check_plain($res->uniquename);
    }
    if (empty($matches)) {
      $result = db_query_range(
        "SELECT feature.uniquename FROM feature feature join cvterm as cvterm ON "
      . " cvterm.cvterm_id=feature.type_id "
      . " WHERE cvterm.name IN ('ORF','polypeptide','contig','gene','mRNA','CDS')", 0, 10
      );
      while ($res = db_fetch_object($result)) {
        $matches[$res->uniquename] = $res->uniquename;
      }
    }
  }
  gmod_dbsf_db_execute();
  drupal_json($matches);
}

/**
 * Autocomplete curation terms
 *
 * @see user_autocomplete
 *
 */
function genes4all_curate_autocomplete_free_term($string = NULL) {
  $matches = array();
  if (!empty($string)) {
    gmod_dbsf_db_execute('chado');
    //Chado specific SQL
    $string = strtolower ( $string );
    $result = db_query_range(
      "SELECT cvterm.name as cvterm_name,cv.name as cv_name FROM cvterm JOIN cv ON cvterm.cv_id = cv.cv_id WHERE cv.name IN "
    ."('free_curation_terms','sequence','biological_process','molecular_function','cellular_component','EC','KEGG_PATHWAY')"
    ." AND LOWER(cvterm.name) like '%%%s%%' ORDER BY cvterm_name", $string, 0, 10
    );
    while ($res = db_fetch_object($result)) {
      $cvterm_name = $res->cvterm_name;
      $cv_name = $res->cv_name;
      $matches[$cv_name.'::'.$cvterm_name] = $cvterm_name;
    }
    /*if (empty($matches)) {
      //Chado specific SQL
      $result = db_query_range(
        "SELECT cvterm.name as cvterm_name,cv.name as cv_name FROM cvterm JOIN cv ON cvterm.cv_id = cv.cv_id WHERE cv.name IN "
      ."('free_curation_terms','sequence','biological_process','molecular_function','cellular_component','EC','KEGG_PATHWAY')"
      ." order by cvterm_name", $string, 0, 10
      );
      while ($res = db_fetch_object($result)) {
        $cvterm_name = $res->cvterm_name;
        $cv_name = $res->cv_name;
        $matches[$cv_name.'::'.$cvterm_name] = $cvterm_name;
      }
    }*/
    gmod_dbsf_db_execute();
  }
  drupal_json($matches);
}

function genes4all_curate_autocomplete_specific_cv($cv_name=NULL,$string = NULL) {
  $matches = array();
  if (empty($cv_name) || empty($string) || strlen($string)<2){
    drupal_json($matches);
    return;
  }
  if (!empty($string)) {
    $string = strtolower ( $string );
    gmod_dbsf_db_execute('chado');
    //Chado specific SQL
    $result = db_query_range(
      "SELECT name,definition FROM cvterm WHERE cv_id = (SELECT cv_id from cv where name = '$cv_name')"
    ." AND (LOWER(name) LIKE '%%%s%%' OR (length(definition)<=80 AND LOWER(definition) LIKE '%%%s%%')) ORDER BY name", $string,$string, 0, 15
    );
    while ($res = db_fetch_object($result)) {
      $name = (!empty($res->definition) && strlen($res->definition)<=80 && preg_match('/^[0-9]/',$res->name)) ? $res->definition : $res->name;
      $matches[$res->name] = str_replace('_',' ',$name);
    }
    gmod_dbsf_db_execute();
  }
  drupal_json($matches);
}

function genes4all_curate_autocomplete_specific_db($db_name=NULL,$string = NULL) {
  $matches = array();
  if (empty($db_name) || empty($string) || strlen($string)<2){
    drupal_json($matches);
    return;
  }
  if (!empty($string)) {
    gmod_dbsf_db_execute('chado');
    $string = strtolower ( $string );
    //Chado specific SQL
    $result = db_query_range(
      "SELECT accession,description FROM dbxref WHERE db_id = (SELECT db_id from db where name = '$db_name')"
    ." AND LOWER(description) like '%%%s%%') ORDER BY description", $string, 0, 10
    );
    while ($res = db_fetch_object($result)) {
      $name = check_plain($res->accession);
      $descr =$res->description;
      $matches[$name] = $descr;
    }
/*    if (empty($matches)) {
      //Chado specific SQL
      $result = db_query_range(
        "SELECT accession,description FROM dbxref WHERE db_id = (SELECT db_id from db where name = '$db_name') ORDER BY accession", $string, 0, 10
      );
      while ($res = db_fetch_object($result)) {
        $name = check_plain($res->accession);
        $descr =$res->description;
        $matches[$name] = $descr;
      }
    }
*/
    gmod_dbsf_db_execute();
  }
  drupal_json($matches);
}

function genes4all_curate_autocomplete_specific_cv_phylosearch($cv_name=NULL,$string = NULL) {
  $matches = array();
  if (empty($cv_name) || empty($string) || strlen($string)<2){
    drupal_json($matches);
    return;
  }
  if (!empty($string)) {
    gmod_dbsf_db_execute('chado');
    $string = strtolower ( $string ) ;
    //Chado specific SQL
    $result = db_query_range(
      "select distinct (cvterm_name) as name,cvterm_organism_mat.cvterm_id,cvterm.definition from cvterm_organism_mat "
      . " join cvterm ON cvterm_organism_mat.cvterm_id=cvterm.cvterm_id WHERE cv_name ='$cv_name' "
    ." AND (LOWER(name) like '%%%s%%' OR (length(definition)<=80 AND LOWER(definition) like '%%%s%%')) ORDER BY name", $string,$string, 0, 15
    );
    while ($res = db_fetch_object($result)) {
      $name = (!empty($res->definition) && strlen($res->definition)<=80 && preg_match('/^[0-9]/',$res->name)) ? $res->definition : $res->name;
      $matches[$res->name] = str_replace('_',' ',$name);
    }
    gmod_dbsf_db_execute();
  }
  drupal_json($matches);
}

function genes4all_curate_autocomplete_specific_db_phylosearch($db_name=NULL,$dbxref_property=NULL,$string = NULL) {
$matches = array();
  if (empty($db_name) || empty($string) || strlen($string)<2){
    drupal_json($matches);
    return;
  }
  $string = strtolower ( $string );
  if (!empty($string)) {
    gmod_dbsf_db_execute('chado');
    //Chado specific SQL
    $result = db_query_range(
    "SELECT distinct ( dbxref_name ),dbxref_id from dbxref_organism_mat where db_name='%s' AND dbxref_property='$dbxref_property' "
    ."AND lower(dbxref_name) LIKE '%%%s%%' ORDER by dbxref_name" ,$db_name, $string,0, 20
    );
    while ($res = db_fetch_object($result)) {
      $descr =$res->dbxref_name;
      $matches[$descr] = $descr;
    }
    gmod_dbsf_db_execute();
  }
  drupal_json($matches);
}


/**
 * Autocomplete library
 *
 * @see user_autocomplete
 *
 */
function genes4all_curate_autocomplete_library_cvterm($string = NULL) {
  $matches = array();
  if (!empty($string)) {
    gmod_dbsf_db_execute('chado');
    //Chado specific SQL
    $string = strtolower ( $string );
    $result = db_query_range(
      "select name from cvterm where cvterm_id IN (select distinct cvterm_id from library_cvterm) AND LOWER(name) LIKE '%s%' order by name", $string, 0, 20
    );
    while ($res = db_fetch_object($result)) {
      $name = $res->name;
      $matches[$name] = $name;
    }
    if (empty($matches)) {
      //Chado specific SQL
      $result = db_query_range(
        "select name from cvterm where cvterm_id IN (select distinct cvterm_id from library_cvterm) order by name", $string, 0, 10
      );
      while ($res = db_fetch_object($result)) {
        $name = $res->name;
        $matches[$name] = $name;
      }
    }
    gmod_dbsf_db_execute();
  }
  drupal_json($matches);
}

/**
 * Autocomplete library dbname
 *
 * @see user_autocomplete
 *
 */
function genes4all_curate_autocomplete_feature_dbname($string = NULL) {
  $matches = array();
  if (!empty($string)) {
    gmod_dbsf_db_execute('chado');
    //Chado specific SQL
    $result = db_query_range(
      "SELECT name from db where lower(name) LIKE LOWER('%s%%') order by name", $string, 0, 10
    );
    while ($res = db_fetch_object($result)) {
      $name = $res->name;
      $matches[$name] = $name;
    }
    gmod_dbsf_db_execute();
  }
  drupal_json($matches);
}


/**
 * Autocomplete library dbname
 *
 * @see user_autocomplete
 *
 */
function genes4all_curate_autocomplete_library_dbname($string = NULL) {
  $matches = array();
  if (!empty($string)) {
    gmod_dbsf_db_execute('chado');
    //Chado specific SQL
    $result = db_query_range(
      "SELECT name from db where db_id IN (select distinct db_id from library_dbxref as ld join dbxref as d on d.dbxref_id=ld.dbxref_id) AND lower(name) LIKE LOWER('%s%%') order by name", $string, 0, 10
    );
    while ($res = db_fetch_object($result)) {
      $name = $res->name;
      $matches[$name] = $name;
    }
    if (empty($matches)) {
      //Chado specific SQL
      $result = db_query_range(
        "SELECT name from db where db_id IN (select distinct db_id from library_dbxref  as ld join dbxref as d on d.dbxref_id=ld.dbxref_id) order by name", $string, 0, 10
      );
      while ($res = db_fetch_object($result)) {
        $name = check_plain($res->name);
        $matches[$name] = $name;
      }
    }
    gmod_dbsf_db_execute();
  }
  drupal_json($matches);
}

/**
 * Autocomplete genus of organism
 *
 * @see user_autocomplete
 *
 */
function genes4all_curate_autocomplete_genus($string = NULL) {
  $matches = array();
  if (!empty($string)) {
    gmod_dbsf_db_execute('chado');
    //Chado specific SQL
    $result = db_query_range(
      "SELECT genus as name FROM organism WHERE lower(genus) LIKE LOWER('%s%%') order by genus", $string, 0, 10
    );
    while ($res = db_fetch_object($result)) {
      $name = $res->name;
      $matches[$name] = $name;
    }
    if (empty($matches)) {
      //Chado specific SQL
      $result = db_query_range(
        "SELECT genus as name FROM organism ORDER BY genus", $string, 0, 10
      );
      while ($res = db_fetch_object($result)) {
        $name = check_plain($res->name);
        $matches[$name] = $name;
      }
    }
    gmod_dbsf_db_execute();
  }
  drupal_json($matches);
}

/**
 * Autocomplete species of organism
 *
 * @see user_autocomplete
 *
 */
function genes4all_curate_autocomplete_species($string = NULL) {
  $matches = array();
  if (!empty($string)) {
    gmod_dbsf_db_execute('chado');
    //Chado specific SQL
    $result = db_query_range(
      "SELECT species as name FROM organism WHERE lower(species) LIKE LOWER('%s%%') order by species", $string, 0, 10
    );
    while ($res = db_fetch_object($result)) {
      $name = $res->name;
      $matches[$name] = $name;
    }
    if (empty($matches)) {
      //Chado specific SQL
      $result = db_query_range(
        "SELECT species as name FROM organism ORDER BY species", $string, 0, 10
      );
      while ($res = db_fetch_object($result)) {
        $name = check_plain($res->name);
        $matches[$name] = $name;
      }
    }
    gmod_dbsf_db_execute();
  }
  drupal_json($matches);
}

/**
 * Autocomplete publication of experiment
 *
 * @see user_autocomplete
 *
 */
function genes4all_experiment_publication_autocomplete($string = '') {
  $passkey = check_plain($_SESSION['passkey']);
  $matches = array();
  if ($string) {
    $result = db_query_range("SELECT uniquename FROM {gmod_dbsf_pub} WHERE LOWER(uniquename) LIKE LOWER('%s%%') AND passkey='$passkey' ORDER BY uniquename", $string, 0, 10);
    while ($res = db_fetch_object($result)) {
      $matches[$res->uniquename] = $res->uniquename;
    }
    if (empty($matches)) {
      $result = db_query_range("SELECT uniquename FROM {gmod_dbsf_pub} order by uniquename", $string, 0, 10);
      while ($res = db_fetch_object($result)) {
        $matches[$res->uniquename] = $res->uniquename;
      }
    }
  }
  drupal_json($matches);
}

/**
 * Autocomplete dbname of experiment
 *
 * @see user_autocomplete
 *
 */
function genes4all_experiment_dbname_autocomplete($string = NULL) {
  $matches = array();
  if (!empty($string)) {
    $result = db_query_range("SELECT name FROM {gmod_dbsf_db} WHERE LOWER(name) LIKE LOWER('%s%%') ORDER BY name", $string, 0, 10);
    while ($res = db_fetch_object($result)) {
      $matches[$res->name] = check_plain($res->name);
    }
    if (empty($matches)) {
      $result = db_query_range("SELECT name FROM {gmod_dbsf_db} order by name", $string, 0, 10);
      while ($res = db_fetch_object($result)) {
        $matches[$res->name] = check_plain($res->name);
      }
    }
  }
  drupal_json($matches);
}

/**
 * Autocomplete target database
 *
 * @see user_autocomplete
 *
 */
function genes4all_experiment_targetdb_autocomplete($string = '') {
  $passkey = check_plain($_SESSION['passkey']);
  $matches = array();
  if (!empty($string) && !empty($passkey)) {
    $result = db_query_range(
      "SELECT uniquename FROM {gmod_dbsf_feature} as experiment_feature join {gmod_dbsf_cvterm} as experiment_cvterm ON "
    . " experiment_cvterm.cvterm_id=experiment_feature.type_id "
    . "  WHERE LOWER(experiment_feature.uniquename) LIKE LOWER('%s%%') AND experiment_cvterm.name='gene_target' AND passkey='$passkey' ORDER BY uniquename", $string, 0, 10
    );
    while ($res = db_fetch_object($result)) {
      $matches[$res->uniquename] = check_plain($res->uniquename);
    }
    if (empty($matches)) {
      $result = db_query_range(
        "SELECT uniquename FROM {gmod_dbsf_feature} as experiment_feature join {gmod_dbsf_cvterm} as experiment_cvterm ON "
      . " experiment_cvterm.cvterm_id=experiment_feature.type_id "
      . "  WHERE experiment_cvterm.name='gene_target' AND passkey='$passkey' order by uniquename", $string, 0, 10
      );
      while ($res = db_fetch_object($result)) {
        $matches[$res->uniquename] = $res->uniquename;
      }
    }
  }
  drupal_json($matches);
}

/**
 * Autocomplete construct database
 *
 * @see user_autocomplete
 *
 */
function genes4all_experiment_constructdb_autocomplete($string = '') {
  $passkey = check_plain($_SESSION['passkey']);
  $matches = array();
  if (!empty($string) && !empty($passkey)) {
    $result = db_query_range(
      "SELECT uniquename FROM {gmod_dbsf_feature} as experiment_feature join {gmod_dbsf_cvterm} as experiment_cvterm ON "
    . " experiment_cvterm.cvterm_id=experiment_feature.type_id "
    . "  WHERE LOWER(experiment_feature.uniquename) LIKE LOWER('%s%%') AND experiment_cvterm.name='rnai_construct' AND passkey='$passkey'  ORDER BY uniquename", $string, 0, 10
    );
    while ($res = db_fetch_object($result)) {
      $matches[$res->uniquename] = check_plain($res->uniquename);
    }
    if (empty($matches)) {
      $result = db_query_range(
        "SELECT uniquename FROM {gmod_dbsf_feature} as experiment_feature join {gmod_dbsf_cvterm} as experiment_cvterm ON "
      . " experiment_cvterm.cvterm_id=experiment_feature.type_id "
      . "  WHERE experiment_cvterm.name='rnai_construct' AND passkey='$passkey' order by uniquename", $string, 0, 10
      );
      while ($res = db_fetch_object($result)) {
        $matches[$res->uniquename] = $res->uniquename;
      }
    }
  }
  drupal_json($matches);
}

/**
 * Autocomplete resource database
 *
 * @see user_autocomplete
 *
 */
function genes4all_experiment_resourcedb_autocomplete($string = '') {
  $passkey = check_plain($_SESSION['passkey']);
  $matches = array();
  if (!empty($string) && !empty($passkey)) {
    $result = db_query_range(
      "SELECT uniquename FROM {gmod_dbsf_resource} ". " WHERE LOWER(uniquename) LIKE LOWER('%s%%') AND passkey='$passkey'  ORDER BY uniquename", $string, 0, 10
    );
    while ($res = db_fetch_object($result)) {
      $matches[$res->uniquename] = check_plain($res->uniquename);
    }
    if (empty($matches)) {
      $result = db_query_range(
        "SELECT uniquename FROM {gmod_dbsf_resource} ". "  WHERE passkey='$passkey' order by uniquename", $string, 0, 10
      );
      while ($res = db_fetch_object($result)) {
        $matches[$res->uniquename] = $res->uniquename;
      }
    }
  }
  drupal_json($matches);
}

/**
 * Autocomplete study database
 *
 * @see user_autocomplete
 *
 */
function genes4all_experiment_studydb_autocomplete($string = '') {
  $matches = array();
  if (!empty($string)) {
    $result = db_query_range(
      "SELECT uniquename FROM {gmod_dbsf_study} " . "  WHERE LOWER(uniquename) LIKE LOWER('%s%%') ORDER BY uniquename", $string, 0, 10
    );
    while ($res = db_fetch_object($result)) {
      $matches[$res->uniquename] = check_plain($res->uniquename);
    }
    if (empty($matches)) {
      $result = db_query_range(
        "SELECT uniquename FROM {gmod_dbsf_study} " . "  order by uniquename", $string, 0, 10
      );
      while ($res = db_fetch_object($result)) {
        $matches[$res->uniquename] = $res->uniquename;
      }
    }
  }
  drupal_json($matches);
}

